<?php

namespace WordPressPluginAPI;

interface ActionHook
{
    /**
     * Subscribes object functions to actions
     *
     * Example returns:
     *     array('action_name' => 'method')
     *     array('action_name' => array('method', $prio))
     *     array('action_name' => array('method', $prio, $args))
     */
    public static function getActions(): array;
}
