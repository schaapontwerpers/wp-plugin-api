<?php

namespace WordPressPluginAPI;

interface FilterHook
{
    /**
     * Subscribe object functions to filters
     *
     * Example returns:
     *     array('filter_name' => 'method')
     *     array('filter_name' => array('method', $prio))
     *     array('filter_name' => array('method', $prio, $args))
     */
    public static function getFilters(): array;
}
