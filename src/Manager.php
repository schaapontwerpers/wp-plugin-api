<?php

/**
 * Register actions and filters to the Plugin API
 */

namespace WordPressPluginAPI;

class Manager
{
    /**
     * Registers an object with the WordPress Plugin API.
     */
    public function register($object)
    {
        if ($object instanceof ActionHook) {
            $this->registerActions($object);
        }

        if ($object instanceof FilterHook) {
            $this->registerFilters($object);
        }
    }

    /**
     * Loops over all actions object should register to
     *
     * Gets called in register()
     */
    private function registerActions(ActionHook $object)
    {
        foreach ($object->getActions() as $name => $parameters) {
            $this->registerAction($object, $name, $parameters);
        }
    }

    /**
     * Loops over all filters object should register to
     *
     * Gets called in register()
     */
    private function registerFilters(FilterHook $object)
    {
        foreach ($object->getFilters() as $name => $parameters) {
            $this->registerFilter($object, $name, $parameters);
        }
    }

    /**
     * Register an object function with a specific action hook.
     *
     * Gets called in registerActions()
     */
    private function registerAction(ActionHook $object, string $name, $parameters)
    {
        if (is_string($parameters)) {
            // Standard registration
            add_action($name, array($object, $parameters));
        } elseif (is_array($parameters) && isset($parameters[0])) {
            // Registration with parameters
            $method = $parameters[0];
            $prio = isset($parameters[1]) ? $parameters[1] : 10;
            $args = isset($parameters[2]) ? $parameters[2] : 1;

            add_action($name, array($object, $method), $prio, $args);
        }
    }

    /**
     * Register an object function with a specific filter hook.
     *
     * Gets called in registerFilters()
     */
    private function registerFilter(FilterHook $object, string $name, $parameters)
    {
        if (is_string($parameters)) {
            // Standard registration
            add_filter($name, array($object, $parameters));
        } elseif (is_array($parameters) && isset($parameters[0])) {
            // Registration with parameters
            $method = $parameters[0];
            $prio = isset($parameters[1]) ? $parameters[1] : 10;
            $args = isset($parameters[2]) ? $parameters[2] : 1;

            add_filter($name, array($object, $method), $prio, $args);
        }
    }
}
